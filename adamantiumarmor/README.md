# How to Patch

1. cd to unpacked directory containing adamantiumarmor plugin
2. Either `delta_plugin apply adamantiumarmor-patch.yaml` or `delta_plugin apply adamantiumarmor-compat-patch.yaml`
3. for every other file in *.patch; do
4. `git apply ${file}.patch`
