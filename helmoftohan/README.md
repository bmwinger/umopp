# How to Patch

1. cd to unpacked directory containing EBQ_Artifact plugin
2. `delta_plugin apply EBQ_Artifact-patch.yaml`
3. `git apply EB_Helm_of_Tohan_mesh.patch`
4. `rm Icons/TX_EB_helm_of_Tohan.tga`
5. `rm Textures/TX_EB_Helm\ of\ Tohan.BMP`
