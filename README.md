# Unofficial Morrowind Official Plugins Patched

This is a repackaged version of UMOPP 3.2.0 by PikachunoTM. The original mod can be found
here: https://www.nexusmods.com/morrowind/mods/43931

This version was designed to be applied automatically by a mod manager such as
[Portmod](https://gitlab.com/portmod/portmod) onto the original assets. It uses
patches to avoid distributing assets that are derivatives of Bethesda assets
from Morrowind, and also splits the mod into separate versions for each plugin.

Each patch file contains a comment describing the changes applied.

## Requirements

The BetterArmors compatibility patch, as well as the plugin patches, require
[DeltaPlugin](https://gitlab.com/bmwinger/delta-plugin) 0.15 or newer.

# Permissions
Used with permission as given on the NexusMods description.

> Feel free to edit and redistribute, but please credit Bethesda for their work, and me for mine.

This version is also released under the same terms.
