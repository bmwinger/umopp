# How to Patch

1. cd to unpacked directory containing bcsounds plugin
2. `delta_plugin apply bcsounds-patch.yaml`
4. `convert Textures/Photodragon.tga Textures/Photodragon.dds`
5. `rm Textures/Photodragon.tga`
6. `git apply Photodragon_mesh.patch`
