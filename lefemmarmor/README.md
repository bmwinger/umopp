# How to Patch

1. cd to unpacked directory containing LeFemmArmor plugin
2. `delta_plugin apply LeFemmArmor-patch.yaml` or `delta_plugin apply LeFemmArmor-compat-patch.yaml`
3. `git apply A_Domina_Helm_mesh.patch`
4. `git apply a_goldArmor_F_H_mesh.patch`
