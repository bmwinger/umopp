# How to Patch

1. cd to unpacked directory containing master index plugin
2. `delta_plugin apply master_index-patch.yaml`
4. `convert Icons/Misc_master_index.tga Icons/Misc_master_index.dds`
5. `rm Icons/Misc_master_index.tga`
6. `git apply master_index_mesh.patch`
